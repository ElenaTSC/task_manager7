package ru.tsk.ilina.tm;

import ru.tsk.ilina.tm.constant.ArgumentConst;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.model.Command;
import ru.tsk.ilina.tm.repository.CommandRepository;
import ru.tsk.ilina.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;


public class Application {

    public static void main(String[] args) {
        System.out.println("**WELCOME TO TASK MANAGER**");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND: ");
            String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final Runtime runtime = Runtime.getRuntime();
        System.out.println("Available processors (cores): " + runtime.availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(runtime.freeMemory()));
        long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + NumberUtil.formatBytes(runtime.totalMemory()));
        final long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Use memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT: showAbout(); break;
            case ArgumentConst.VERSION: showVersion(); break;
            case ArgumentConst.HELP: showHelp(); break;
            case ArgumentConst.INFO: showInfo(); break;
            default: showErrorCommand();
        }
    }

    public static void showErrorCommand() {
        System.out.println("Error! Command not found");
    }

    public static void showErrorArgument() {
        System.out.println("Error! Argument not supported");
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT: showAbout(); break;
            case TerminalConst.VERSION: showVersion(); break;
            case TerminalConst.HELP: showHelp(); break;
            case TerminalConst.EXIT: exit(); break;
            case TerminalConst.INFO: showInfo(); break;
        }
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Elena Ilina");
        System.out.println("E-MAIL: eilina@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getCommands();
        for (final Command command : commands) System.out.println(command);
    }

}